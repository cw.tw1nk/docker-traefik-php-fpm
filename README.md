Install docker: https://docs.docker.com/engine/install/

Install docker-compose: https://docs.docker.com/compose/install/

```bash
firewall-cmd --permanent --direct --add-rule ipv4 filter INPUT 4 -i docker0 -j ACCEPT
firewall-cmd --reload
systemctl restart docker
```

To start the container run the command  
`docker-compose -f proxy/docker-compose.yml up -d`

`docker-compose up -d nginx`
