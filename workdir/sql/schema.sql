use web;
drop table if exists email_confirmation;
drop table if exists auth_session;
drop table if exists reset_password;
drop table if exists user;
drop table if exists session;
create table user(
                     id int PRIMARY KEY AUTO_INCREMENT,
                     email varchar(128) CHARACTER SET utf8 UNIQUE NOT NULL,
                     password varchar(60) NOT NULL,
                     privileged BOOLEAN not null default false,
                     active BOOLEAN not null default false,
                     create_at timestamp NOT NULL
) ENGINE=InnoDB;
create table email_confirmation(
                                   id int primary key auto_increment,
                                   user_id int,
                                   hash varchar(32),
                                   constraint fk_user_mail_verification_user
                                       foreign key (user_id)
                                           references user(id) on DELETE NO ACTION
) ENGINE=InnoDB;
create table session(
                        id int PRIMARY KEY AUTO_INCREMENT,
                        user_agent varchar(255),
                        ip VARCHAR(39) CHARACTER SET utf8 DEFAULT NULL,
                        time timestamp,
                        referrer VARCHAR(2083) CHARACTER SET 'ascii' COLLATE 'ascii_general_ci' default null,
                        session VARCHAR(32) unique
) ENGINE=InnoDB;
create table auth_session
(
    id int PRIMARY KEY AUTO_INCREMENT,
    user_id int,
    session_id int,
    token varchar(32) unique,
    constraint fk_user_auth_session foreign key (user_id) references user(id)  ON DELETE SET NULL,
    constraint fk_session_auth_session foreign key (session_id) references session(id)  ON DELETE SET NULL
) ENGINE=InnoDB;
create table reset_password(
                               id int primary key auto_increment,
                               user_id int,
                               token varchar(32),
                               constraint fk_user_reset_password
                                   foreign key (user_id)
                                       references user(id) on DELETE NO ACTION
) ENGINE=InnoDB;

create table roles(
  id int primary key auto_increment,
  role_name varchar(64)
) ENGINE=InnoDB;

create table permission(
    id int primary key auto_increment,
    permission_name varchar(64)
) ENGINE=InnoDB;

create table users_roles(
    user_id int,
    role_id int,
    constraint pk_users_roles primary key (user_id,role_id),
    constraint fk_user_roles foreign key (user_id) references user(id),
    constraint fk_role_users foreign key (role_id) references roles(id)
) ENGINE=InnoDB;

create table users_permissions(
    user_id int,
    permission_id int,
    constraint pk_users_permissions primary key (user_id,permission_id),
    constraint fk_user_permissions foreign key (user_id) references user(id),
    constraint fk_permission_users foreign key (permission_id) references permission(id)
) ENGINE=InnoDB;

create table roles_permissions(
    role_id int,
    permission_id int,
    constraint pk_users_permissions primary key (role_id,permission_id),
    constraint fk_role_permissions foreign key (role_id) references roles(id),
    constraint fk_permission_roles foreign key (permission_id) references permission(id)
) ENGINE=InnoDB;

-- login: admin@admin.com
-- password: admin123
INSERT INTO web.user (id, email, password, privileged, active, create_at) VALUES (1, 'admin@admin.com', '$2y$10$xy0VBCkCqK2AB3E/pF7qDeH4A787n3LmMZCledmlSmfGD2xpXRx/.', 1, 1, '2019-06-08 08:18:49');
